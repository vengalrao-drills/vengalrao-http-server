const http = require("http");
const PORT = 5000;
const { v4: uuidv4 } = require("uuid");

let server = http.createServer((req, res) => {
  if (req.method == "GET") {
    // if my method is get- then it enters. there are diff types like get Request , PUT Request , POST Request . here we need fetching the data - get
    if (req.url == "/html") {
      res.statusCode = 200;
      res.setHeader("Content-type", "text/html"); // i want to send the html file  -  so content-type is (text/html)
      res.write(
        // this can be viewd in browser
        `<!DOCTYPE html>
            <html>
              <head>
              </head>
              <body>
                  <h1>Any fool can write code that a computer can understand. Good programmers write code that humans can understand.</h1>
                  <p> - Martin Fowler - - - - </p>
            
              </body>
            </html>`
      );
      res.end();
    } else if (req.url == "/json") {
      // if my url is  hostname/json
      res.statusCode = 200;
      res.setHeader("Content-type", "application/json"); // i want to send json data -> so, content-type -> application/json// below in res.write( here jsoin data is sended to th ebrowser)
      res.write(`{
                "slideshow": {
                  "author": "Yours Truly",
                  "date": "date of publication",
                  "slides": [
                    {
                      "title": "Wake up to WonderWidgets!",
                      "type": "all"
                    },
                    {
                      "items": [
                        "Why <em>WonderWidgets</em> are great",
                        "Who <em>buys</em> WonderWidgets"
                      ],
                      "title": "Overview",
                      "type": "all"
                    }
                  ],
                  "title": "Sample Slide Show"
                }
              } `);
      res.end();
    } else if (req.url == "/uuid") {
      // if my url is  hostname:PORT/uuid
      let generatedUuid = uuidv4(); // generating a uuid
      res.statusCode = 200;
      res.setHeader("Content-type", "text/plain"); // it is of json format
      res.write(`{  "uuid":${JSON.stringify(generatedUuid)} }`);
      res.end();
    } else if (
      req.url.split("/")[1] == "status" &&
      !isNaN(req.url[req.url.length - 1])
    ) {
      // if my url locahost:PORT/status/___ then it's true
      let url = req.url.split("/");
      console.log(url.length);
      let status_code = url[url.length - 1]; // checking the status code
      console.log("status_code ", status_code);
      if (url.length == 3) {
        console.log(req.url);
        console.log(req.url.split("/"));
        res.statusCode = statusCode;
        res.setHeader("Content-type", "text/plain");
        res.write(`{ Return a response with ${status_code} status code  }`);
        res.end();
      }
    } else if (
      req.url.split("/")[1] == "delay" &&
      !isNaN(req.url[req.url.length - 1])
    ) {
      // checking the base url --it suould include delay word
      let baseUrl = req.url;
      let url = baseUrl.split("/");
      let delay_in_seconds = url[url.length - 1]; // checking the length in dley in seconds
      setTimeout(() => {
        if (url.length == 3) {
          res.statusCode = 200;
          res.setHeader("Content-type", "text/plain");
          res.write(`response with ${delay_in_seconds} status code`);
          res.end();
        }
      }, delay_in_seconds * 1000); // it will wait for seconds and the respose will come
    } else {
      /// if error in the above url that is that the http request is invalid. then, this will occur.
      res.statusCode = 300;
      res.setHeader("Content-type", "text/html");
      res.write(
        `<h1 style={text-align="center"}>404 Error - Invalid Status Code</h1>`
      );
      res.end();
    }
  }
});

server.listen(PORT, () => {
  //server listening on port, ()=>{ } callback function
  console.log(`Server running on ${PORT}`);
});
